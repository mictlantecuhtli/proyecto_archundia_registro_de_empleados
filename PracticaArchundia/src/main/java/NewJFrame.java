/*
Autor: Fatima Azucena MC
Fecha: 24_11_2020
Correo: fatimaazucenamartinez274@gmail.com
*/

//Importacion de paquetes
import CalculoEdad.Calculador;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class NewJFrame extends javax.swing.JFrame {//Inicio clase principal
    //Con el DefaultTableModel podemos agragar las filas que se ingresen
    DefaultTableModel modelo;
    //Declaración de variables
    String sexo;
    String interes;
    public NewJFrame() {
        initComponents();
        //Agregamos los datos que quremos que se muestren en la tabla
        modelo=new DefaultTableModel();
        modelo.addColumn("Nombre");
        modelo.addColumn("Apellido");
        modelo.addColumn("Año");
        modelo.addColumn("Edad");
        modelo.addColumn("Sexo");
        modelo.addColumn("Areas");
        this.table.setModel(modelo);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jTextEdad = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jRFemenino = new javax.swing.JRadioButton();
        jRMasculino = new javax.swing.JRadioButton();
        jLabel7 = new javax.swing.JLabel();
        jCFut = new javax.swing.JCheckBox();
        jCVol = new javax.swing.JCheckBox();
        jCOtra = new javax.swing.JCheckBox();
        jTOtro = new javax.swing.JTextField();
        jBInsertar = new javax.swing.JButton();
        jBEliminar = new javax.swing.JButton();
        jBEliminarTodo = new javax.swing.JButton();
        jTextNombre = new javax.swing.JTextField();
        jTextApellidos = new javax.swing.JTextField();
        jComboNacimiento = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setText("Registro de empleados");

        jLabel2.setText("Nombre:");

        jLabel3.setText("Apellidos:");

        jLabel4.setText("Año de nacimiento");

        jLabel5.setText("Edad:");

        jLabel6.setText("Sexo");

        jRFemenino.setText("Femenino");
        jRFemenino.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jRFemeninoMousePressed(evt);
            }
        });

        jRMasculino.setText("Masculino");
        jRMasculino.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jRMasculinoMousePressed(evt);
            }
        });

        jLabel7.setText("Areas de interes:");

        jCFut.setText("Futbol");
        jCFut.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jCFutMousePressed(evt);
            }
        });

        jCVol.setText("Voleiball");
        jCVol.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jCVolMousePressed(evt);
            }
        });

        jCOtra.setText("Otro");
        jCOtra.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jCOtraMousePressed(evt);
            }
        });

        jBInsertar.setText("Insertar");
        jBInsertar.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                jBInsertarComponentResized(evt);
            }
        });
        jBInsertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBInsertarActionPerformed(evt);
            }
        });

        jBEliminar.setText("Eliminar");
        jBEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBEliminarActionPerformed(evt);
            }
        });

        jBEliminarTodo.setText("Eliminar todo");
        jBEliminarTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBEliminarTodoActionPerformed(evt);
            }
        });

        jComboNacimiento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020" }));
        jComboNacimiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboNacimientoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextApellidos)
                            .addComponent(jTextNombre)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(18, 18, 18)
                                .addComponent(jCFut))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(jBInsertar)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jCVol)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jCOtra))
                            .addComponent(jBEliminar))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jBEliminarTodo))
                            .addComponent(jTOtro, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jRMasculino)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addGap(11, 11, 11)
                            .addComponent(jComboNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jLabel5)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextEdad, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jLabel6)
                            .addGap(18, 18, 18)
                            .addComponent(jRFemenino))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jTextNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jTextApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jTextEdad, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jRFemenino)
                    .addComponent(jComboNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jRMasculino)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(jCFut)
                        .addComponent(jCVol)
                        .addComponent(jCOtra))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jTOtro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jBInsertar)
                    .addComponent(jBEliminarTodo)
                    .addComponent(jBEliminar))
                .addGap(0, 11, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6"
            }
        ));
        jScrollPane3.setViewportView(table);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBInsertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBInsertarActionPerformed
        // TODO add your handling code here:
        //Llenado de datos en la tabla
       String []dato=new String [6];
        dato[0]=jTextNombre.getText();
        dato[1]=jTextApellidos.getText();
        dato[2]=jComboNacimiento.getSelectedItem().toString();
        dato[3]=jTextEdad.getText();
        dato[4]=sexo;
        
        //Condicional if la cual si el boton de "Otro" esta habilitado vamos a
        //poder obtener el valor que el usuario ingrese en la caja de texto
        if (jCOtra.isEnabled()){
            interes=jTOtro.getText();
        }
        dato[5]=interes;
    
        //Agregar los datos
        modelo.addRow(dato);
        //Reseteo de campos en la interfaz
        jTextNombre.setText("");
        jTextApellidos.setText("");
        jComboNacimiento.addItem("");
        jTextEdad.setText("");
      
    }//GEN-LAST:event_jBInsertarActionPerformed

    private void jBEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBEliminarActionPerformed
        // TODO add your handling code here:
        //Esta condicional if indica que si el numero de filas que se han ingresado
        //es mayor a 0 va a poder remover la que quiera, siempre y cuando la seleccione
         int fil=table.getSelectedRow();
        if (fil>=0){//Inicio if-else
            modelo.removeRow(fil);
        }
        else{//La parte del else servira en caso de el usuario no seleccione la
            //fila que quiere eliminar, le mandara un mensaje diceiendo que 
            //seleccione la fila a eliminar
            JOptionPane.showMessageDialog(null, "Seleccione la fila a eliminar");
        }//Fin if-else
    }//GEN-LAST:event_jBEliminarActionPerformed

    private void jBEliminarTodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBEliminarTodoActionPerformed
        // TODO add your handling code here:
        //Mediante el siguiente for lograremos borrar todas las filas que han
        //sido ingresadas
         int fil=table.getRowCount();
        for (int i = fil; i >=0; i--) {//Inicio for
            modelo.removeRow(i);
            
        }//Fin for
    }//GEN-LAST:event_jBEliminarTodoActionPerformed

    private void jComboNacimientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboNacimientoActionPerformed
        // TODO add your handling code here:
        //Crearemos un objeto para poder llamar a otra clase
        Calculador objCalculador = new Calculador();
        objCalculador.Calculador2();
        //Declararremos 2 variables de tipo entero que almacenaran el valor que 
        //obtenga
        int valor, edad;
        //Mediante la siguiente linea de código almacenaremos el valor que el
        //usuario selecciono en el jComboBox
        valor = Integer.parseInt(jComboNacimiento.getSelectedItem().toString());
        //Llamaremos al objeto que hemos creado y al metodo de la otra clase, el
        //cual hara el preceso de calculo de la edad
        edad = objCalculador.getEdad(valor);jTextEdad.setText(""+edad);
    }//GEN-LAST:event_jComboNacimientoActionPerformed

    private void jRFemeninoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jRFemeninoMousePressed
        // TODO add your handling code here:
           //Si se selecciona el boton de "Femenino" desabilita el otro boton y
           //viceversa
            sexo="Femenino";
            jRMasculino.setSelected(false);
       
    }//GEN-LAST:event_jRFemeninoMousePressed

    private void jRMasculinoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jRMasculinoMousePressed
        // TODO add your handling code here:
            //Si se selecciona el boton de "Masculino" dasabilita en automatico
            //el otro boton y viceversa
            sexo="Masculino";
            jRFemenino.setSelected(false);
       
    }//GEN-LAST:event_jRMasculinoMousePressed

    private void jCFutMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCFutMousePressed
        // TODO add your handling code here:
        //Si se selecciona el boton de "Futbol" se deshabilitan los botones
        //restantes
        interes="Futbol";
        jCVol.setSelected(false);
        jCOtra.setSelected(false);
    }//GEN-LAST:event_jCFutMousePressed

    private void jCVolMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCVolMousePressed
        // TODO add your handling code here:
        //Si se selecciona el boton de "Volieball" se deshabilita los 2 restantes
        interes="Voleiball";
        jCFut.setSelected(false);
        jCOtra.setSelected(false);
    }//GEN-LAST:event_jCVolMousePressed

    private void jCOtraMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCOtraMousePressed
        // TODO add your handling code here:
        //Si se selecciona el boton de otros se deshabilitan los dos restantes
        interes=jTOtro.getText();
        jCFut.setSelected(false);
        jCVol.setSelected(false);
    }//GEN-LAST:event_jCOtraMousePressed

    private void jBInsertarComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jBInsertarComponentResized
        // TODO add your handling code here:
    }//GEN-LAST:event_jBInsertarComponentResized

    public void Empleados (){
           java.awt.EventQueue.invokeLater(new Runnable() {
               public void run() {
                new NewJFrame().setVisible(true);
                }
            });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBEliminar;
    private javax.swing.JButton jBEliminarTodo;
    private javax.swing.JButton jBInsertar;
    private javax.swing.JCheckBox jCFut;
    private javax.swing.JCheckBox jCOtra;
    private javax.swing.JCheckBox jCVol;
    private javax.swing.JComboBox<String> jComboNacimiento;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JRadioButton jRFemenino;
    private javax.swing.JRadioButton jRMasculino;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTOtro;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTextField jTextApellidos;
    private javax.swing.JTextField jTextEdad;
    private javax.swing.JTextField jTextNombre;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
}//Fin clase principal
